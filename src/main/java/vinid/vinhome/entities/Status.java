package vinid.vinhome.entities;
// Generated Dec 23, 2019 10:25:41 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Status generated by hbm2java
 */
@Entity
@Table(name = "status", catalog = "vinid_home")
public class Status implements java.io.Serializable {

	private Integer statusId;
	private String statusCode;
	private int statusName;
	private Date createdOn;
	private String createdBy;
	private Set<User> users = new HashSet<User>(0);
	private Set<AdressHome> adressHomes = new HashSet<AdressHome>(0);
	private Set<PaymentProvider> paymentProviders = new HashSet<PaymentProvider>(0);
	private Set<Role> roles = new HashSet<Role>(0);
	private Set<Building> buildings = new HashSet<Building>(0);
	private Set<Home> homes = new HashSet<Home>(0);

	public Status() {
	}

	public Status(String statusCode, int statusName, Date createdOn, String createdBy) {
		this.statusCode = statusCode;
		this.statusName = statusName;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
	}

	public Status(String statusCode, int statusName, Date createdOn, String createdBy, Set<User> users,
			Set<AdressHome> adressHomes, Set<PaymentProvider> paymentProviders, Set<Role> roles,
			Set<Building> buildings, Set<Home> homes) {
		this.statusCode = statusCode;
		this.statusName = statusName;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.users = users;
		this.adressHomes = adressHomes;
		this.paymentProviders = paymentProviders;
		this.roles = roles;
		this.buildings = buildings;
		this.homes = homes;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "status_id", unique = true, nullable = false)
	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	@Column(name = "status_code", nullable = false, length = 20)
	public String getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Column(name = "status_name", nullable = false)
	public int getStatusName() {
		return this.statusName;
	}

	public void setStatusName(int statusName) {
		this.statusName = statusName;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_on", nullable = false, length = 10)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "created_by", nullable = false, length = 200)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<AdressHome> getAdressHomes() {
		return this.adressHomes;
	}

	public void setAdressHomes(Set<AdressHome> adressHomes) {
		this.adressHomes = adressHomes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<PaymentProvider> getPaymentProviders() {
		return this.paymentProviders;
	}

	public void setPaymentProviders(Set<PaymentProvider> paymentProviders) {
		this.paymentProviders = paymentProviders;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<Building> getBuildings() {
		return this.buildings;
	}

	public void setBuildings(Set<Building> buildings) {
		this.buildings = buildings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	public Set<Home> getHomes() {
		return this.homes;
	}

	public void setHomes(Set<Home> homes) {
		this.homes = homes;
	}

}
