package vinid.vinhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(scanBasePackages = "vinid.vinhome")
@EnableCaching
public class VinHomeWebServiceApplication {
    public static void main(String[] args){
        SpringApplication.run(VinHomeWebServiceApplication.class,args);
    }
}
